import { Router } from 'express';
import { BaseController } from '../controllers/base.controller';

const BaseRouter = Router();
const Controller = new BaseController();

// * GET /api/auth
BaseRouter.get('/', Controller.index);

export { BaseRouter };
