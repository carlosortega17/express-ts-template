import { Service } from 'typedi';
import { createLogger, transports, format } from 'winston';

@Service()
export class Logger {
  public logger = createLogger({
    transports: [new transports.Console()],
    format: format.combine(
      format.colorize(),
      format.timestamp(),
      format.printf(({ timestamp, level, message }) => `[${timestamp}] ${level}: ${message}`),
    ),
  });
}
