import './configs/dependencies';
import express from 'express';
import { Container } from 'typedi';
import { Logger } from 'winston';
import { AppDataSource } from './configs/database';
import { setupGlobalMiddlewares } from './configs/app';
import { setupApiRoutes } from './configs/api';

const logger = Container.get<Logger>('logger');

AppDataSource.initialize().then(() => {
  logger.info('Database connected successfully');
}).catch((err) => {
  logger.error(err);
  throw err;
});

const app = express();

setupGlobalMiddlewares(app);
setupApiRoutes(app);

export default app;
