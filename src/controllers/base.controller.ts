import { Request, Response } from 'express';

export class BaseController {
  private code = 200;

  public index = (_: Request, res: Response) => res.status(this.code).json({ message: 'Success' });
}
