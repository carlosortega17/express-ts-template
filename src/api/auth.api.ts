import { Router } from 'express';
import { Container } from 'typedi';
import { Repository } from 'typeorm';
import { User } from 'src/models';
import { Logger } from 'winston';
import { AuthController } from '../controllers/auth.controller';

const AuthRouter = Router();
const UserModel = Container.get<Repository<User>>('UserModel');
const logger = Container.get<Logger>('logger');
const Controller = new AuthController(UserModel, logger);

// * GET /api/auth
AuthRouter.get('/', Controller.index);

// * POST /api/auth
AuthRouter.post('/', Controller.login);

// * GET /api/test
AuthRouter.get('/test', Controller.test);

export { AuthRouter };
