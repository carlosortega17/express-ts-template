import { Request, Response } from 'express';
import { validate } from 'class-validator';
import { Repository } from 'typeorm';
import { Inject, Service } from 'typedi';
import { Logger } from 'winston';
import { User } from '../models/user.model';
import { LoginDto } from '../interfaces/dto/user.dto';

@Service()
export class AuthController {
  private temp = 'message';

  constructor(
    @Inject('UserModel') private readonly userModel: Repository<User>,
    @Inject('logger') private readonly logger: Logger,
  ) {}

  public index = (_: Request, res: Response) => res.status(200).json({ message: this.temp });

  public async login(req: Request<unknown, unknown, LoginDto>, res: Response) {
    const body = new LoginDto(req.body);
    const errors = await validate(body);
    if (errors.length > 0) {
      return res.status(400).json({ errors });
    }
    const user = await this.userModel.findOneBy({ email: body.email });
    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }
    const isPasswordValid = user.password === body.password;
    if (!isPasswordValid) {
      return res.status(400).json({ message: 'Password is invalid' });
    }
    return res.status(200).json({ message: 'Login success' });
  };

  public test = async (_: Request, res: Response) => {
    const user = await this.userModel.insert({
      firstName: 'Test',
      lastName: 'Test',
      email: 'email@email.com',
      password: '123456',
      isActive: true,
    });
    this.logger.info(user);
    return res.status(200).json({
      message: 'test',
    });
  };
}
