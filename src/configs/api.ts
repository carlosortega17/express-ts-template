import type { Application } from 'express';
import { AuthRouter } from '../api/auth.api';

export const setupApiRoutes = (app: Application): void => {
  app.use('/api/auth', AuthRouter);
};
