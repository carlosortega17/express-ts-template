import { Container } from 'typedi';
import * as models from '../models';
import { AppDataSource } from './database';
import { Logger } from '../services/logger.service';

const dependencies = () => {
  const { logger } = new Logger();
  Container.set('logger', logger);
  try {
    Object.keys(models).forEach((model) => {
      Container.set(`${model}Model`, AppDataSource.getRepository(model));
      logger.info(`${model}Model injected successfully`);
    });
    logger.info('Dependencies injected successfully');
  } catch (error) {
    logger.error(error);
    throw error;
  }
};

dependencies();

export {};
