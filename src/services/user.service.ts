import { Inject, Service } from 'typedi';

@Service()
export class UserService {
  constructor(
    @Inject() private readonly userModel: Record<string, unknown>,
  ) {}

  public async login() {
    return Promise.resolve(this.userModel.data);
  }
}
